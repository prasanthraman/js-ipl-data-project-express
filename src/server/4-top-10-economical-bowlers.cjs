const csvParser = require('csv-parser')
const fs = require('fs')

function topTenEconomicalBowlers(callback) {

    let runsConcededByBowler = {}
    let { matchesPath, deliveriesPath, outputPath } = require('./paths.cjs')
    let matchesFileStream = fs.createReadStream(matchesPath)

    getMatchIdsForYear('2015', matchesFileStream)

    function getMatchIdsForYear(year, matchesFileStream) {
        let matchIds = []
        matchesFileStream.pipe(csvParser())
            .on('data', (data) => {
                if (data.season == year) {
                    matchIds.push(data.id)
                }
            })
            .on('end', () => {
                //console.log(matchIds)
                onCompletecallback(matchIds, year)

                return matchIds
            })

    }

    function onCompletecallback(matchIds, year) {
        fs.createReadStream(deliveriesPath)
            .pipe(csvParser())
            .on('data', (data) => {
                if (matchIds.includes(data.match_id)) {
                    runsConcededByBowler = countOccurence(data, runsConcededByBowler)
                }
            })
            .on('end', () => {
                runsConcededByBowler = calculateEconomy(runsConcededByBowler)
                console.log(JSON.stringify(runsConcededByBowler))
                fs.createWriteStream(`${outputPath}4-Top-10-economical-bowlers-of-${year}.json`)
                    .write(JSON.stringify(runsConcededByBowler))
                callback(null,runsConcededByBowler)
            })
    }

    function calculateEconomy(runsConcededByBowler) {
        let obj = {}
        let players = Object.keys(runsConcededByBowler)
            .filter((_, index) => {
                return (index % 2 == 0)
            })
        let factors = Object.values(runsConcededByBowler)
            .filter((_, index) => {
                return !(index % 2 == 0)
            })
            .map(balls => parseFloat(balls / 6))
        let economy = Object.values(runsConcededByBowler)
            .filter((_, index) => {
                return (index % 2 == 0)
            })
            .map((runs, index) => {
                return parseFloat(runs / factors[index]).toFixed(2)
            })
        players = players.map((player, index) => {
            obj[player] = economy[index]
            return obj
        })
        obj = Object.fromEntries(Object.entries(obj)
            .sort(([, bowler], [, bowlerOne]) => bowler - bowlerOne))
        obj = Object.keys(obj)
            .slice(0, 10)
            .reduce((result, key) => {
                result[key] = obj[key]
                return result
            }, {})
        return obj
    }

    function countOccurence(data, runsConcededByBowler) {
        if (data.bowler != "") {
            if (Object.keys(runsConcededByBowler).includes(data.bowler)) {
                runsConcededByBowler[data.bowler] += parseInt(data.total_runs)
                runsConcededByBowler[`balls_bowled_by_${data.bowler}`] += 1
            }
            else {
                runsConcededByBowler[data.bowler] = parseInt(data.total_runs)
                runsConcededByBowler[`balls_bowled_by_${data.bowler}`] = 1
            }
        }
        return runsConcededByBowler
    }
}

module.exports = topTenEconomicalBowlers