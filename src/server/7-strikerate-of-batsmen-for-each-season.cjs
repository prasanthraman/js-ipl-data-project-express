const csvParser = require('csv-parser')
const fs = require('fs')

function strikeRateForEachSeason(callback) {

    let strikeRateSeason = {}
    let { matchesPath, deliveriesPath, outputPath } = require('./paths.cjs')
    let matchesFileStream = fs.createReadStream(matchesPath)

    matchesFileStream
        .pipe(csvParser())
        .on('data', (data) => {
            if (Object.keys(strikeRateSeason).includes(data.season)) {
                strikeRateSeason[data.season].push(data.id)
            }
            else {
                strikeRateSeason[data.season] = []
                strikeRateSeason[data.season].push(data.id)
            }
        })
        .on('end', () => {
            console.log((strikeRateSeason))
            let seasons = Object.keys(strikeRateSeason)
            let matchIds = Object.values(strikeRateSeason)
            computeStrikeRate(seasons, matchIds)

        })
    function computeStrikeRate(seasons, matchIds) {
        //console.log(matchIds)
        let batsmen = {}
        let deliveriesFileStream = fs.createReadStream(deliveriesPath)
        deliveriesFileStream.pipe(csvParser())
            .on('data', (data) => {
                let season = findseason(data.match_id)
                //console.log(season,data.match_id)
                if (Object.keys(batsmen).includes(season)) {
                    batsmen[season] = compute(data, batsmen[season])
                } else {
                    batsmen[season] = compute(data, {})
                }
            })
            .on('end', () => {
                Object.keys(batsmen).reduce((result, key) => {
                    //console.log(batsmen[key])
                    Object.keys(batsmen[key])
                        .reduce((resultOne, keyOne) => {
                            let strikeRate = batsmen[key][keyOne]
                            strikeRate = parseFloat((strikeRate[0] / strikeRate[1]) * 100).toFixed(2)
                            batsmen[key][keyOne] = strikeRate
                            return resultOne
                        }, {})
                    return result
                }, {})
                console.log(batsmen)
                fs.createWriteStream(`${outputPath}7-strikerate-of-batsmen-for-each-year.json`)
                    .write(JSON.stringify(batsmen))
                callback(null,batsmen)
            })

        function findseason(id) {
            let requiredIndex
            matchIds.map((currentValue, currentIndex) => {
                currentValue.map(indexOfCurrentValue => {

                    if (indexOfCurrentValue == id) {
                        requiredIndex = currentIndex
                    }
                })
            })
            return seasons[requiredIndex]

        }
        function compute(data, batsmen) {
            if (Object.keys(batsmen).includes(data.batsman)) {
                batsmen[data.batsman][0] += parseInt(data.batsman_runs)
                batsmen[data.batsman][1] += 1

            } else {
                batsmen[data.batsman] = [parseInt(data.batsman_runs), 1]
            }
            return batsmen
        }
    }
}



module.exports = strikeRateForEachSeason