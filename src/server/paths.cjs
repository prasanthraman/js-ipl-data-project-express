const path=require('path')

const matchesPath=path.join(__dirname,'../data/matches.csv')
const deliveriesPath=path.join(__dirname,'../data/deliveries.csv')
const outputPath=path.join(__dirname,'../public/output/')

module.exports= {matchesPath,deliveriesPath,outputPath}