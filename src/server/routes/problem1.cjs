const express = require('express')
const router = express.Router()

const matchesPerYear = require('../1-matches-per-year.cjs')

router.get('/', (req, res) => {
    matchesPerYear((err, data) => {
        if (err) {
            res.send(err)
        } else {
            res.send(data)
        }
    })
})

module.exports = router
