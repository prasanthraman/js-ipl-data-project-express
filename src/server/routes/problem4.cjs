const express = require('express')
const router = express.Router()

const economicalBowlers = require('../4-top-10-economical-bowlers.cjs')

router.get('/', (req, res) => {
    economicalBowlers((err, data) => {
        if (err) {
            res.send(err)
        } else {
            res.send(data)
        }
    })
})

module.exports = router