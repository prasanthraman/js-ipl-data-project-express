const express = require('express')
const router = express.Router()

const playerDismissals = require('../8-higher-number-of-times-player-dismissed-by-another-player.cjs')

router.get('/', (req, res) => {
    playerDismissals((err, data) => {
        if (err) {
            res.send(err)
        } else {
            res.send(data)
        }
    })
})

module.exports = router