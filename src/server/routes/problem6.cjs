const express = require('express')
const router = express.Router()

const playerOfMatch = require('../6-player-of-match-each-season.cjs')

router.get('/', (req, res) => {
    playerOfMatch((err, data) => {
        if (err) {
            res.send(err)
        } else {
            res.send(data)
        }
    })
})

module.exports = router