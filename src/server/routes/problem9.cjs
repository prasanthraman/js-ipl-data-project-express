const express = require('express')
const router = express.Router()

const highestEconomy = require('../9-player-with-highest-economy.cjs')

router.get('/', (req, res) => {
    highestEconomy((err, data) => {
        if (err) {
            res.send(err)
        } else {
            res.send(data)
        }
    })
})

module.exports = router