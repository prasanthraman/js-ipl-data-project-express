const express = require('express')
const router = express.Router()

const wonTossWonMatch = require('../5-won-toss-won-match.cjs')

router.get('/', (req, res) => {
    wonTossWonMatch((err, data) => {
        if (err) {
            res.send(err)
        } else {
            res.send(data)
        }
    })
})

module.exports = router