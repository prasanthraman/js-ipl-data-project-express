const express = require('express')
const router = express.Router()

const matchesWonPerTeam = require('../2-matches-won-per-team-per-year.cjs')

router.get('/', (req, res) => {
    matchesWonPerTeam((err, data) => {
        if (err) {
            res.send(err)
        } else {
            res.send(data)
        }
    })
})

module.exports = router