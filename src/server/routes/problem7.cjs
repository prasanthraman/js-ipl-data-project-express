const express = require('express')
const router = express.Router()

const strikeRateEachSeason = require('../7-strikerate-of-batsmen-for-each-season.cjs')

router.get('/', (req, res) => {
    strikeRateEachSeason((err, data) => {
        if (err) {
            res.send(err)
        } else {
            res.send(data)
        }
    })
})

module.exports = router