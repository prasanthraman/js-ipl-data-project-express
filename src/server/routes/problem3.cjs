const express = require('express')
const router = express.Router()

const extraRuns = require('../3-extra-runs-conceded-per-team.cjs')

router.get('/', (req, res) => {
    extraRuns((err, data) => {
        if (err) {
            res.send(err)
        } else {
            res.send(data)
        }
    })
})

module.exports = router