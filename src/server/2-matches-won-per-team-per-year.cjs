const csvParser = require('csv-parser')
const fs = require('fs')

function matchesWonPerTeamPerYear(callback) {
    let { matchesPath, outputPath } = require('./paths.cjs')
    let matchesWonPerTeam = {}
    let counter = 1

    fs.createReadStream(matchesPath)
        .pipe(csvParser())
        .on('data', (data) => {
            if (Object.keys(matchesWonPerTeam).includes(data.season)) {
                matchesWonPerTeam[data.season] = countOccurence(data, matchesWonPerTeam[data.season])
            } else {
                matchesWonPerTeam[data.season] = countOccurence(data, {})
            }

            console.log(matchesWonPerTeam)
        })
        .on('end', () => {
            console.log(JSON.stringify(matchesWonPerTeam))
            fs.createWriteStream(`${outputPath}2-matches-won-per-team.json`)
                .write(JSON.stringify(matchesWonPerTeam))
            callback(null,matchesWonPerTeam)
        })

    function countOccurence(data, matchesWonPerTeam) {
        if (data.winner != "") {
            if (Object.keys(matchesWonPerTeam).includes(data.winner)) {
                matchesWonPerTeam[data.winner]++
            }
            else {
                matchesWonPerTeam[data.winner] = counter
            }
        }
        return matchesWonPerTeam
    }
}

module.exports = matchesWonPerTeamPerYear