const csvParser = require('csv-parser')
const fs = require('fs')

function extraRunsPerTeam(callback) {

    let runsConcededPerTeam = {}
    let { matchesPath, deliveriesPath, outputPath } = require('./paths.cjs')
    let matchesFileStream = fs.createReadStream(matchesPath)

    getMatchIdsForYear('2016', matchesFileStream)

    function getMatchIdsForYear(year, matchesFileStream) {
        let matchIds = []
        matchesFileStream.pipe(csvParser())
            .on('data', (data) => {
                if (data.season == year) {
                    matchIds.push(data.id)
                }
            })
            .on('end', () => {
                console.log(matchIds)
                onCompleteCallback(matchIds, year)
                return matchIds
            })
    }

    function onCompleteCallback(matchIds, year) {
        fs.createReadStream(deliveriesPath)
            .pipe(csvParser())
            .on('data', (data) => {
                if (matchIds.includes(data.match_id)) {
                    runsConcededPerTeam = countOccurence(data, runsConcededPerTeam)
                }
            })
            .on('end', () => {
                console.log(JSON.stringify(runsConcededPerTeam))
                fs.createWriteStream(`${outputPath}3-extra-runs-conceded-per-team-${year}.json`)
                    .write(JSON.stringify(runsConcededPerTeam))
                callback(null,runsConcededPerTeam)
            })
    }

    function countOccurence(data, runsConcededPerTeam) {

        if (data.bowling_team != "") {
            if (Object.keys(runsConcededPerTeam).includes(data.bowling_team)) {
                runsConcededPerTeam[data.bowling_team] += parseInt(data.extra_runs)
            }
            else {
                runsConcededPerTeam[data.bowling_team] = parseInt(data.extra_runs)
            }
        }

        return runsConcededPerTeam
    }
}

module.exports = extraRunsPerTeam